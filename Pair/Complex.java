public class Complex extends Pair {

    public Complex(double f, double s) {
        super(f, s);
        this.f = f;
        this.s = s;
    }

    public Complex add(Pair p) {
        return new Complex(getF() + p.getF(), getS() + p.getS());
    }

    public Complex sub(Pair p) {
        return new Complex(getF() - p.getF(), getS() - p.getS());
    }

    public Complex mult(Pair p) {
        Complex result = new Complex(
                (getF() * p.getF()) - (getS() * p.getS()), (getF() * p.getS()) + (getS() * p.getF()));
        return result;
    }

    public Complex div(Pair p) {
        double denominator = (p.getF() * p.getF()) + (p.getS() * p.getS());
        double realPart = (getF() * p.getF() + getS() * p.getS()) / denominator;
        double imagePart = (getS() * p.getF() - getF() * p.getS()) / denominator;
        return new Complex(realPart, imagePart);
    }
    public String toString() {
        return f + " + " + s + "i";
    }
}

