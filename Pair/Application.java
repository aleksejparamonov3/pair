public class Application {
    public static void main(String[] args) {
        Complex s = new Complex(2.0, 0.0);
        Complex p = new Complex(2.0, 0.0);

        Rational a = new Rational(2.0, 2.0);
        Rational b = new Rational(1.0, 1.0);

        System.out.println("Комплексные числа");
        System.out.println("s = " + s);
        System.out.println("p = " + p);
        System.out.println();
        System.out.println("Результат сложения комплексных чисел = " + s.add(p));
        System.out.println("Результат вычитания комплексных чисел = " + s.sub(p));
        System.out.println("Результат умножения комплексных чисел = " + s.mult(p));
        System.out.println("Результат деления комплексных чисел = " + s.div(p));
        System.out.println();
        System.out.println("Рациональные числа");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println();
        System.out.println("Результат сложения рациональных чисел = " + a.add(b));
        System.out.println("Результат вычитания рациональных чисел = " + a.sub(b));
        System.out.println("Результат умножения рациональных чисел = " + a.mult(b));
        System.out.println("Результат деления рациональных чисел = " + a.div(b));
    }
}
