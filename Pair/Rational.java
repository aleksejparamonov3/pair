public class Rational extends Pair {

    public Rational(double f, double s) {
        super(f, s);
        this.f = f;
        this.s = s;
    }

    public Rational add(Pair p) {
        return new Rational(getF() * p.getS() + getS() * p.getF(), getS() * p.getS());
    }

    public Rational sub(Pair p) {
        return new Rational(getF() * p.getS() - getS() * p.getF(), getS() * p.getS());
    }

    public Rational mult(Pair p) {
        return new Rational(getF() * p.getF(), getS() * p.getS());
    }

    public Rational div(Pair p) {
        return new Rational(getF() * p.getS(), getS() * p.getF());
    }

    public String toString() {
        return f + " / " + s;
    }
}



