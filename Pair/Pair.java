public abstract class Pair {
  protected double f;
  protected double s;

  public Pair(double f, double s) {
    this.f = f;
    this.s = s;
  }

  public double getF() {
    return f;
  }

  public double getS() {
    return s;
  }

  abstract public Pair add(Pair p);

  abstract public Pair sub(Pair p);

  abstract public Pair mult(Pair p);

  abstract public Pair div(Pair p);
}